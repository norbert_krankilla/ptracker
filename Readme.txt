Payment Tracker is an application with maven structure.

Howto:

1. Build application
  mvn install

2. Run application (with default initial balance)
  mvn exec:java


Alternative ways how to run the Payment Tracker
  2.1 Run application without initial balance
    mvn -DnoInitialBalance exec:java
  2.2 Run application with an alternative initial balance file
    mvn exec:java -Dexec.args="alternative.balance"