package com.bsc.ptracker;

import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parser for the input line. The input line is expected in form "XXX 123"
 * @author Norbert Krankilla
 */
public class InputParser {
	
	private final Logger logger = LoggerFactory.getLogger(InputParser.class);
	
	/**
	 * Parse input line
	 * @param line Line to be parsed
	 * @param parsingConsumer Consumer which consumes the result of successful parsing
	 * @throws ValidationException in case the line is not in expected pattern
	 */
	public void parse(String line, ParsingConsumer parsingConsumer) throws ValidationException{
		logger.debug("Scanning line {}", line);
		Scanner scanner = new Scanner(line);
		
		String currency;
		if (!scanner.hasNext()) {
			logger.error(Messages.getString("error.currencyMissing"));
			throw new ValidationException(Messages.getString("error.currencyMissing"));
		} else if (scanner.hasNext("^[a-zA-Z]{3}")) {
			currency = scanner.next().toUpperCase();
		} else {
			String m = String.format(Messages.getString("error.currencyInvalid"), scanner.next());
			logger.error(m);
			throw new ValidationException(m);
		}
		
		int amount;
		if (!scanner.hasNext()) {
			logger.error(Messages.getString("error.amountMissing"));
			throw new ValidationException(Messages.getString("error.amountMissing"));
		} else if (scanner.hasNextInt()) {
			amount = scanner.nextInt();
		} else {
			String m = String.format(Messages.getString("error.amountInvalid"), scanner.next());
			logger.error(m);
			throw new ValidationException(m);
		}
		
		if (scanner.hasNext()) {
			logger.error(Messages.getString("error.tooManyArguments"));
			throw new ValidationException(Messages.getString("error.tooManyArguments"));
		}
		logger.info("Parsing done. Currency={}, Amount={}.", currency, amount);
		parsingConsumer.consume(currency, amount);
	}
}
