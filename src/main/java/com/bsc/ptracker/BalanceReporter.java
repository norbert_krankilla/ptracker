package com.bsc.ptracker;

import java.io.PrintWriter;

/**
 * Job, which is scheduled to report the balance
 * @author Norbert Krankilla
 */
public class BalanceReporter implements Runnable {

	private final PrintWriter pw;

	public BalanceReporter(PrintWriter pw) {
		this.pw = pw;
	}

	@Override
	public void run() {
		PaymentManager pm = PaymentManager.getInstance();
		pw.println(Messages.getString("actualBalance"));
		pm.getSnapshot().forEach((String t, Integer u) -> {
			pw.printf("\t%s %d\n", t, u);
		});
		pw.flush();
	}

}
