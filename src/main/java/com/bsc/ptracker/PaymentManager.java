package com.bsc.ptracker;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains the balance map and provides operations over it.
 * @author Norbert Krankilla
 */
public class PaymentManager {

	private final Logger logger = LoggerFactory.getLogger(PaymentManager.class);
	
	private final Map<String, Integer> balance;
	private static PaymentManager instance;
	
	// Singleton
	private PaymentManager() {
		// This will ensure the listing will have the same order each time
		this.balance = new ConcurrentHashMap<>();
		logger.debug("Payment manager created.");
	}
	
	
	/**
	 * Get the single instance of this class
	 * @return 
	 */
	public static PaymentManager getInstance() {
		if (instance == null) {
			instance = new PaymentManager();
		}
		return instance;
	}
	

	/**
	 * Creates new copy of the balance. T
	 * his way the consumer cannot change the balance and the snapshot is 
	 * no more affected by {@link #addPayment(java.lang.String, java.lang.Integer)}
	 * @return 
	 */
	public Map<String, Integer> getSnapshot() {
		Map<String, Integer> copy = new HashMap<>(balance.size());
		balance.entrySet().stream().forEach((entry) -> {
			copy.put(entry.getKey(), entry.getValue());
		});
		logger.debug("Snapshot of balance created.");
		return copy;
	}
	
	
	/**
	 * Creating new record in balance. 
	 * In case the currency exists, the amount is sum of old and new value.
	 * In case the sum of some currency is 0, the record is removed from balance.
	 * @param currency
	 * @param amount 
	 */
	public void addPayment (String currency, Integer amount) {
		Integer actual = balance.get(currency);
		actual = (actual==null) ? 0 : actual;
		actual += amount;
		if (actual.equals(0)) {
			balance.remove(currency);
			logger.info("Currency {} was removed.", currency);
		} else {
			balance.put(currency, actual);
			logger.info("Currency {} was updated to {}.", currency, amount);
		}
	} 
}
