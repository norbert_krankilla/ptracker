package com.bsc.ptracker;

/**
 * This interface serves as a callback for the {@link InputParser}.
 * @author Norbert Krankilla
 */
public interface ParsingConsumer {
	
	/**
	 * The implementation should process the currency and amount which are the result of parsing.
	 * @param currency in form XXX
	 * @param amount is a decimal number
	 */
	void consume(String currency, int amount);
	
}
