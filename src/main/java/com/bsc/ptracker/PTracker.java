package com.bsc.ptracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import jline.console.ConsoleReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Payment Tracker application main class
 * @author soja
 */
public class PTracker {

	/**
	 * System property name to skip loading default initial balance
	 */
	public static final String NO_INITIAL_BALANCE_PROPERTY = "noInitialBalance";

	private final Logger logger = LoggerFactory.getLogger(PTracker.class);
	private final ScheduledExecutorService scheduler;
	private final ScheduledFuture<?> reporterHandler;
	private final ConsoleReader reader;
	private final PrintWriter out;
	private final PaymentManager pm;
	private final InputParser parser;

	
	public static void main(String[] args) throws Exception {
		PTracker ptracker = new PTracker();
		if (args.length > 0) {
			ptracker.loadInitialBalance(args[0]);
		}
		ptracker.doMyJob();

		ptracker.shutdown();
	}

	
	public PTracker() throws IOException {
		this.pm = PaymentManager.getInstance();
		this.parser = new InputParser();
		this.reader = new ConsoleReader();
		this.out = new PrintWriter(reader.getOutput());
		
		// Start new Thread
		this.scheduler = Executors.newScheduledThreadPool(1);
		this.reporterHandler = scheduler.scheduleAtFixedRate(
				new BalanceReporter(out), 
				60, 60, TimeUnit.SECONDS);

		this.reader.setPrompt(Messages.getString("prompt"));
		this.reader.clearScreen();
	}
	

	/**
	 * This method is doing the main loop and processing the input lines.
	 * @throws IOException 
	 */
	public void doMyJob() throws IOException {
		out.println("************************************");
		out.println(Messages.getString("welcome"));
		out.println(Messages.getString("help"));
		out.println("************************************");
		
		String line;
		while ((line = reader.readLine()) != null) {
			
			// quit
			if ("quit".equals(line)) {
				break;
			} else if ("help".equals(line)) {
				out.println(Messages.getString("help"));
				continue;
			}
			// if only "enter" pressed
			else if (line.trim().isEmpty()) {
				continue;
			}
			
			// parse and process line
			try {
				processInputLine(line);
			} catch (ValidationException ex) {
				out.printf("Error: %s\n", ex.getMessage());
				logger.error("Cannot parse line {}: {}", line, ex.getMessage());
			}
		}
	}
	

	/**
	 * Provide shutdown of all threads of application
	 */
	public void shutdown() {
		reporterHandler.cancel(true);
		scheduler.shutdown();
		reader.shutdown();
	}

	
	/**
	 * Load the initial balance from the given file
	 * @param filename
	 * @throws FileNotFoundException 
	 */
	private void loadInitialBalance(String filename) throws FileNotFoundException {
		List<String> lines = null;

		// Read File with initial data
		if (!System.getProperties().containsKey(NO_INITIAL_BALANCE_PROPERTY)) {
			logger.debug("Loading initial balance from file {}", filename);
			File initialFile = new File(filename);
			if (!initialFile.exists() || !initialFile.canRead()) {
				String m = String.format(Messages.getString("error.cannotOpenFile"), initialFile.getAbsolutePath());
				out.print(m);
				logger.error(m);
				System.exit(1);
			}
			BufferedReader fileReader = new BufferedReader(new FileReader(initialFile));
			fileReader.lines().forEach(s -> {
				try {
					processInputLine(s);
				} catch (ValidationException ex) {
					String m = String.format(Messages.getString("error.wrongLineInFile"), filename, s, ex.getMessage());
					out.print(m);
					logger.error(m);
					System.exit(0);
				}
				logger.info("Loading of initial balance done.");
			});
		} else {
			logger.info("Loading of initial balance skipped.");
		}
	}


	/**
	 * Process the input line by adding the parsed valued into PaymentManager
	 * @param line
	 * @throws ValidationException 
	 */
	private void processInputLine(String line) throws ValidationException {
		parser.parse(line, (String currency, int amount) -> {
			pm.addPayment(currency, amount);
			out.printf(Messages.getString("addedInfo"), amount, currency);
		});
	}
}
