package com.bsc.ptracker;

import java.util.ResourceBundle;

/**
 * This us a wrapper for ResourceBundle containing messages
 * @author Norbert Krankilla
 */
public class Messages {

	private static final ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle");
	
	/**
	 * Returns message from the bundle
	 * @param key
	 * @return 
	 */
	public static String getString(String key) {
		return messages.getString(key);
	}
	
}
