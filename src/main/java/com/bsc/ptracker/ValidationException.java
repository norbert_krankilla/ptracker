package com.bsc.ptracker;

/**
 *
 * @author soja
 */
public class ValidationException extends Exception{
	
	public ValidationException(String message) {
		super(message);
	}
	
}
