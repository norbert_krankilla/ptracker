package com.bsc.ptracker;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class InputParserTest {
	
	public InputParserTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	@Test
	public void testParse() throws Exception {
		// Main Flow
		String line = "CUR 123";
		ParsingConsumer parsingConsumer = new ParsingConsumer() {
			@Override
			public void consume(String currency, int amount) {
				assertEquals("CUR", currency);
				assertEquals(123, amount);
			}
		};
		InputParser instance = new InputParser();
		instance.parse(line, parsingConsumer);
	}

	
	@Test(expected = ValidationException.class)
	public void testParse_missingCurrency() throws Exception {
		new InputParser().parse("", (a,b)->{});
	}

	@Test(expected = ValidationException.class)
	public void testParse_invalidCurrency() throws Exception {
		new InputParser().parse("a1 123", (a,b)->{});
	}

	@Test(expected = ValidationException.class)
	public void testParse_missingAmount() throws Exception {
		new InputParser().parse("aaa", (a,b)->{});
	}

	@Test(expected = ValidationException.class)
	public void testParse_invalidAmount() throws Exception {
		new InputParser().parse("aaa aa", (a,b)->{});
	}

	@Test(expected = ValidationException.class)
	public void testParse_tooManyArguments() throws Exception {
		new InputParser().parse("aaa 123 123", (a,b)->{});
	}
}
