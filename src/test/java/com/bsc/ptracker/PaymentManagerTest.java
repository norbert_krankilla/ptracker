package com.bsc.ptracker;

import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PaymentManagerTest {
	
	public PaymentManagerTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getInstance method, of class PaymentManager.
	 */
	@Test
	public void testGetInstance() {
		System.out.println("getInstance");
		PaymentManager expResult = PaymentManager.getInstance();
		PaymentManager result = PaymentManager.getInstance();
		assertEquals(expResult, result); // The same instance
	}

	/**
	 * Test of getSnapshot method, of class PaymentManager.
	 */
	@Test
	public void testAddAndGetSnapshot() throws Exception{
		System.out.println("getSnapshot");
		PaymentManager instance = PaymentManager.getInstance();
		Map<String, Integer> empty = instance.getSnapshot();
		assertNotNull(empty);
		assertEquals(0, empty.size());
		
		// Creating record
		instance.addPayment("AAA", 123);
		Map<String, Integer> result = instance.getSnapshot();
		assertNotNull(result);
		assertEquals(1, result.size());
		String currency = result.keySet().iterator().next();
		assertEquals("AAA", currency);
		assertEquals(123, result.get(currency).intValue());
		
		// incrementing record
		instance.addPayment("AAA", 5);
		Map<String, Integer> result2 = instance.getSnapshot();
		assertEquals(1, result2.size());
		assertEquals(128, result2.get("AAA").intValue());
		
		// Removing zero record
		instance.addPayment("AAA", -128);
		Map<String, Integer> result3 = instance.getSnapshot();
		assertEquals(0, result3.size());
	}
	
}
